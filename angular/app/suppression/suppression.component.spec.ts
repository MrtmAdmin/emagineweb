/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import {TestBed, async, ComponentFixture, tick, fakeAsync} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {SuppressionComponent} from './suppression.component';
import {SharedModule} from '../shared/shared.module';
import {SuppressionService} from './suppression.service';
import {Suppression} from './suppression';
import {CustomerContextRecordsModule} from '../customercontextrecords/customer-context-records.module';
import {LoginComponent} from '../login/login.component';
import {DashboardComponent} from '../dashboard/dashboard.component';
import {EventDetectorModule} from '../eventdetectors/event-detector.module';
import {UsecaseComponent} from '../usecases/usecases.component';
import {DeploymentRequestComponent} from '../deploymentrequest/deploymentrequest.component';
import {DeploymentComponent} from '../deploymentrequest/deployment.component';
import {PendingApprovalComponent} from '../deploymentrequest/pendingapproval.component';
import {ActionsComponent} from '../actions/actions.component';
import {SegmentComponent} from '../segments/segment.component';
import {BaseRequestOptions} from '@angular/http';
import {MockBackend} from '@angular/http/testing';
import {BrowserModule} from '@angular/platform-browser';
import {Configuration} from '../services/configuration';
import {APP_BASE_HREF} from '@angular/common';
import {fakeBackendProvider} from '../helpers/fake-backend';
import {NoopAnimationsModule, BrowserAnimationsModule} from '@angular/platform-browser/animations';
import * as _ from 'lodash';

describe('SuppressionComponent', () => {
    let suppressionCompInstance: SuppressionComponent;
    let fixture: ComponentFixture<SuppressionComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                NoopAnimationsModule,
                BrowserAnimationsModule,
                SharedModule,
                BrowserModule,
                CustomerContextRecordsModule,
                EventDetectorModule
            ],
            declarations: [
                SuppressionComponent,
                LoginComponent,
                DashboardComponent,
                UsecaseComponent,
                DeploymentRequestComponent,
                DeploymentComponent,
                PendingApprovalComponent,
                ActionsComponent,
                SegmentComponent
            ],
            providers: [
                SuppressionService,
                Configuration,
                MockBackend,
                fakeBackendProvider,
                BaseRequestOptions,
                {provide: APP_BASE_HREF, useValue: '/'}
            ]
        }).compileComponents();

    }));

    it('should create the app', async(() => {
        fixture = TestBed.createComponent(SuppressionComponent);
        suppressionCompInstance = fixture.debugElement.componentInstance;
        expect(suppressionCompInstance).toBeTruthy();
    }));
    
    it('Set Tab Parameter', async(() => {
        console.log("Set Tab Parameter");
        fixture = TestBed.createComponent(SuppressionComponent);
        suppressionCompInstance = fixture.debugElement.componentInstance;
        suppressionCompInstance.setTabParameters('config', 2);
        expect(suppressionCompInstance.selectedTab).toEqual('config');
    }));
    
    it('Computing Rule Expression', async(() => {
        console.log("Computing Rule Expression");
        fixture = TestBed.createComponent(SuppressionComponent);
        suppressionCompInstance = fixture.debugElement.componentInstance;
        let str = suppressionCompInstance.computed({"operator":"AND","rules":[{"condition":"=","field":"Temp Name - -1524061161","data":"36"},{"condition":"=","field":"supp-update","data":"96"}]});
        expect(str).toEqual('(Temp Name - -1524061161 = 36 <strong>AND</strong> supp-update = 96)');
    }));
    
    it('Add Suppressions', function (done) {
        console.log("Add Suppression");
        fixture = TestBed.createComponent(SuppressionComponent);
        suppressionCompInstance = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            suppressionCompInstance.setTabParameters('config', 2);
            fixture.detectChanges();
            const originalLength = suppressionCompInstance.configurationSuppressionList.length;
            let suppression = new Suppression();
            suppression.suppressionId = null;
            suppression.suppressionName = "R-RECH_CHECK_TARIFF_ID_ST-ADD";
            suppression.suppressionDesc = "Suppress if TARIFF ID does not start with 100 or 200";
            suppression.failureLogLabel = "TARIF ID 100 or 200";
            suppression.successLogLabel = "TARIF ID 100 or 200";
            suppression.version = 1.20;
            suppression.status = "Published";
            suppression.isInSimulation = false;
            suppression.isInProduction = false;
            suppression.updatable = true;
            suppression.criteriaExpression = "{\"operator\":\"AND\",\"rules\":[{\"condition\":\"=\",\"field\":\"supp add\",\"data\":\"63\"}]}";
            suppressionCompInstance.suppressionObj = suppression;
            suppressionCompInstance.addOrUpdateSuppression();
            setTimeout(function () {
                fixture.detectChanges();
                expect(suppressionCompInstance.configurationSuppressionList.length).toEqual(originalLength + 1);
                done();
            }, 5);
        });

    });

    it('Update Suppressions', function (done) {
        console.log("Update Suppression");
        fixture = TestBed.createComponent(SuppressionComponent);
        suppressionCompInstance = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            suppressionCompInstance.setTabParameters('config', 2);
            fixture.detectChanges();
            suppressionCompInstance.selectedIndex = suppressionCompInstance.configurationSuppressionList.length - 1;
            suppressionCompInstance.suppressionObj = _.cloneDeep(suppressionCompInstance.configurationSuppressionList[suppressionCompInstance.configurationSuppressionList.length - 1]);
            suppressionCompInstance.suppressionObj.suppressionName = "R-RECH_CHECK_TARIFF_ID_ST-UPDATE";
            suppressionCompInstance.addOrUpdateSuppression();
            setTimeout(function () {
                fixture.detectChanges();
                expect(suppressionCompInstance.configurationSuppressionList[suppressionCompInstance.configurationSuppressionList.length - 1].suppressionName).toEqual('R-RECH_CHECK_TARIFF_ID_ST-UPDATE');
                done();
            }, 5);
        });
    });
    
    it('Delete Suppressions', function (done) {
        console.log("Delete Suppression");
        fixture = TestBed.createComponent(SuppressionComponent);
        suppressionCompInstance = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            suppressionCompInstance.setTabParameters('config', 2);
            fixture.detectChanges();
            const originalLength = suppressionCompInstance.configurationSuppressionList.length;
            suppressionCompInstance.modalContent = _.cloneDeep(suppressionCompInstance.configurationSuppressionList[suppressionCompInstance.configurationSuppressionList.length - 1]);
            suppressionCompInstance.deleteSuppression();
            setTimeout(function () {
                fixture.detectChanges();
                expect(suppressionCompInstance.configurationSuppressionList.length).toEqual(originalLength - 1);
                done();
            }, 500);
        });
    });
    
    it('Archive Suppressions', function (done) {
        console.log("Archive Suppression");
        fixture = TestBed.createComponent(SuppressionComponent);
        suppressionCompInstance = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            suppressionCompInstance.setTabParameters('config', 2);
            fixture.detectChanges();
            const originalLength = suppressionCompInstance.configurationSuppressionList.length;
            suppressionCompInstance.modalContent = _.cloneDeep(suppressionCompInstance.configurationSuppressionList[suppressionCompInstance.configurationSuppressionList.length - 1]);
            suppressionCompInstance.archiveSuppression();
            setTimeout(function () {
                fixture.detectChanges();
                expect(suppressionCompInstance.configurationSuppressionList.length).toEqual(originalLength - 1);
                done();
            }, 500);
        });
    });
    
});


