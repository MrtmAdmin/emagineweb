import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'filterarray'
})

export class FilterArrayPipe implements PipeTransform {
    transform(items: any, args: any): any {
        if (args == '' || args == undefined) {return items;}
        let query = args.toLowerCase();
        return items.filter(item => item.name.toLowerCase().indexOf(query) !== -1);
    }
}