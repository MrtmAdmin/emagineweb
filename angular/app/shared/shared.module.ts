/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {DataTableModule} from 'primeng/primeng';
import {DialogModule} from 'primeng/primeng';
import {GrowlModule} from 'primeng/primeng';
import {DndModule} from 'ng2-dnd';
import {NumbersOnly} from '../directives/numbers-only';
import {MultiselectDropdownModule} from '../directives/dropdown-multiselect/multiselect-dropdown';
import {CalendarModule} from 'primeng/primeng';
import {QueryBuilderComponent} from '../directives/query-builder';
import {FilterArrayPipe} from '../pipes/filter-array.pipe';
import {MapToIterable} from '../pipes/map-to-Iterable.pipe';
import {ContextMenuModule} from '../directives/angular2-contextmenu/angular2-contextmenu';
import {ContextMenuService} from '../directives/angular2-contextmenu/src/contextMenu.service';
import {SpinnerComponent} from '../directives/spinner-component/spinner.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        DataTableModule,
        DndModule.forRoot(),
        CalendarModule,
        DialogModule,
        GrowlModule,
        ContextMenuModule,
        MultiselectDropdownModule
    ],
    exports: [
        CommonModule, 
        FormsModule,
        DataTableModule,
        DndModule,
        CalendarModule,
        DialogModule,
        GrowlModule,
        MultiselectDropdownModule,
        FilterArrayPipe,
        MapToIterable,
        NumbersOnly,
        QueryBuilderComponent,
        SpinnerComponent,
        ContextMenuModule
    ],
    declarations: [
        QueryBuilderComponent,
        NumbersOnly,
        FilterArrayPipe,
        MapToIterable,
        SpinnerComponent
    ],
    providers: [ContextMenuService]
})
export class SharedModule {}
