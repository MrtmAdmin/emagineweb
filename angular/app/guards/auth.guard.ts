import {Injectable} from '@angular/core';
import {Router, CanActivate, CanDeactivate} from '@angular/router';
import {ConfirmationService} from 'primeng/primeng';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate, CanDeactivate<any> {

    constructor(private router: Router, private confirmationService: ConfirmationService) {}
    
    canActivate() {
        if (localStorage.getItem('currentUser')) {
            // logged in so return true
            return true;
        }

        // not logged in so redirect to login page
        this.router.navigate(['/login']);
        return false;
    }

    canDeactivate(target: any) {
        if (target.hasChanges()) {
            return Observable.create((observer: Observer<boolean>) => {
                this.confirmationService.confirm({
                    message: 'You have unsaved changes. Are you sure you want to leave this page?',
                    accept: () => {
                        observer.next(true);
                        observer.complete();
                    },
                    reject: () => {
                        observer.next(false);
                        observer.complete();
                    }
                });
            });
        } else {
            return true;
        }
    }
}