import {Directive, Input} from '@angular/core';
import {NgModel} from '@angular/forms';
/*  Name of Directive   :   numbersOnly
 *  Function            :   It allows the user to enter only numbers. (0-9)
 *
 *  How to use?
 *
 *  1. Import this class to the component file which is going to use this directive.
 *  2. Mention this directive in the @Component metadata of the component file.
 *      directives  :   [NumbersOnly]
 *  3. Just write [numbersOnly] as an attribute of your HTML input element
 *     (Info :  The HTML component needs to have an id.)
 *  4. If you want to allow decimal number, you need to specify [numbersOnly]="'decimal'" in the tag.
 *  5. If you want to set the limit of the number specify the number in maxNumber="2" attribute
 *
 *      e.g. <input id="box" [(ngModel)]="number" maxNumber="2" [numbersOnly]="'decimal'" type="text">
 */



@Directive({
  selector: '[numbersOnly]',
  host: {
    '(input)' : 'isNumeric($event)',
    '(change)'    : 'isNumeric($event)'
  },
  providers : [NgModel]
})

export class NumbersOnly
{
    @Input('numbersOnly') inputType: string;
    tmp: any;

    @Input() maxNumber: number;

    constructor(private model: NgModel){

    }

    isNumeric(event: any)
    {
            this.tmp=$("#"+event.target.id).val();
            if (this.inputType=="decimal")
            {
                this.tmp=this.tmp.replace(/[A-z]/g,"");
                this.tmp=this.tmp.replace(/[-|!@#$%^&*(){}=<,>:;"'?+*"]/g,"");
                this.tmp=this.tmp.replace("/","");

                while((this.tmp.split(".").length - 1)>1)
                {
                    this.tmp=this.tmp.replace(".","");
                }
                event.target.value = this.tmp;
                //document.getElementById(event.target.id).value = this.tmp;
                // this.box.value=val;
            }
            else
            {
                this.tmp=this.tmp.replace(/[A-z]/g,"");
                this.tmp=this.tmp.replace(/[-|.!@#$%^&*(){}=<,>:;"'?+*"]/g,"");
                this.tmp = this.tmp.replace("/","");
                event.target.value = this.tmp;
                this.model.viewToModelUpdate(this.tmp);
                //document.getElementById(event.target.id).value = this.tmp;
                //box.value=val;
            }

            if (this.maxNumber != null){
                if (parseInt(this.tmp) > this.maxNumber){
                    event.target.value = this.maxNumber;
                }
            }


    }
}




/*onInput(val: any)
    {
        if (this.inputType=="decimal")
        {
          val=val.replace(/[A-z]/g,"");
          val=val.replace(/[-|!@#$%^&*(){}=<,>:;"'?+*"]/,"");
          val=val.replace("/","");
          //document.getElementById("box").value=val;
          this.box.value=val;
        }
        else
        {
            val=val.replace(/[A-z]/g,"");
            val=val.replace(/[-|.!@#$%^&*(){}=<,>:;"'?+*"]/,"");
            val=val.replace("/","");
            //document.getElementById("box").value=val;
            box.value=val;
        }
    }*/

/*if (this.inputType=="decimal")
        {

           if(event.which==46)
           {
               this.tmp = document.getElementById(event.target.id).value;
               if (this.tmp.indexOf(".")==-1)
               {
                 return true;
               }
               else
               {
                   return false;
               }
           }

           else if ((event.which >= 48 && event.which <= 57) || event.which==8 || event.which==46)
           {
                return true;
           }

           else
           {
                return false;
           }
        }
        else
        {
            if ((event.which >= 48 && event.which <= 57) || event.which==8)
            {
                return true;
            }
            else
            {
                return false;
            }
        }*/
    
