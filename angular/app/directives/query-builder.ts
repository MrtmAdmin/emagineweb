import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'query-builder',
    templateUrl: 'query-builder.html'

})
export class QueryBuilderComponent implements OnInit {
    @Input() group: any;
    @Input() parentGrp: any;
    @Input() grpIndex: number;

    @Output() groupChange = new EventEmitter();
    queryBuild: QueryBuilderComponent;
    constructor() {}
    ngOnInit() {
    }

    operators = [
        {name: 'AND'},
        {name: 'OR'}
    ];

    fields = [
        {name: 'Firstname'},
        {name: 'Lastname'},
        {name: 'Birthdate'},
        {name: 'City'},
        {name: 'Country'}
    ];

    conditions = [
        {name: '='},
        {name: '<>'},
        {name: '<'},
        {name: '<='},
        {name: '>'},
        {name: '>='}
    ];

    addCondition() {
        this.group.rules.push({
            condition: '=',
            field: '',
            data: ''
        });
        this.setGroup();
    };

    removeCondition(index) {
        this.group.rules.splice(index, 1);
        this.setGroup();
    };

    addGroup() {
        this.group.rules.push({
            group: {
                operator: 'AND',
                rules: []
            }
        });
        this.setGroup();

    };

    setGroup() {
        this.groupChange.emit(this.group);
    }

    removeGroup() {
        this.parentGrp.rules.splice(this.grpIndex, 1);
        this.setGroup();
    };

    ongroupChanged(val) {
        if (this.group.group === undefined || this.group.group === null) {
            this.group.group = {};
        }
        Object.assign(this.group.group, val);
        this.setGroup();
    }

}