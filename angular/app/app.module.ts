import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule, BaseRequestOptions,XHRBackend} from '@angular/http';
import './rxjs-extensions';
import {AppComponent} from './app.component';
import {AppRoutingModule, routedComponents} from './app-routing.module';
import {HeaderComponent} from './headers/header.component';
import {FooterComponent} from './footers/footer.component';
////// used to create fake backend
//import {fakeBackendProvider} from './helpers/fake-backend';
//import {MockBackend} from '@angular/http/testing';
import {AuthGuard} from './guards/auth.guard';
import {AuthenticationService} from './services/authentication.service';
import {LoginComponent} from './login/login.component';
import {EventDetectorModule} from './eventdetectors/event-detector.module';
import {SuppressionModule} from './suppression/suppression.module';
import {UsecaseModule} from './usecases/usecases.module';
import {DeploymentRequestModule} from './deploymentrequest/deploymentrequest.module';
import {FeedsModule} from './feeds/feeds.module';
import {ConsumerModule} from './consumer/consumer.module';
import {CustomerContextRecordsModule} from './customercontextrecords/customer-context-records.module';
import {ActionsModule} from './actions/actions.module';
import {SegmentModule} from './segments/segment.module';
import {HttpService} from './services/http-interceptor.service';
import {Configuration} from './services/configuration';
import { HashLocationStrategy, LocationStrategy} from '@angular/common';
import {ConfirmDialogModule, ConfirmationService} from 'primeng/primeng';
import {ToasterComponent, ToastComponent} from 'angular2-toastr/index';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        SuppressionModule,
        HttpModule,
        EventDetectorModule,
        UsecaseModule,
        DeploymentRequestModule,
        FeedsModule,
        ConsumerModule,
        CustomerContextRecordsModule,
        ActionsModule,
        SegmentModule,
        ConfirmDialogModule
    ],
    declarations: [
        AppComponent,
        LoginComponent,
        HeaderComponent,
        FooterComponent,
        routedComponents,
        ToasterComponent,
        ToastComponent
    ],
    providers: [
        AuthGuard,
        AuthenticationService,
        Configuration,
        ConfirmationService,
        {provide: LocationStrategy, useClass: HashLocationStrategy},
        {
            provide: HttpService,
            deps: [XHRBackend, BaseRequestOptions],
            useFactory: (backend, options) => {
                return new HttpService(backend, options);
            }
        },
//        fakeBackendProvider,
//        MockBackend,
        BaseRequestOptions
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
