
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginComponent} from './login/login.component';
import {EventDetectorComponent} from './eventdetectors/eventdetectors.component';
import {SuppressionComponent} from './suppression/suppression.component';
import {UsecaseComponent} from './usecases/usecases.component';
import {DeploymentRequestComponent} from './deploymentrequest/deploymentrequest.component';
import {DeploymentComponent} from './deploymentrequest/deployment.component';
import {PendingApprovalComponent} from './deploymentrequest/pendingapproval.component';
import {ActionsComponent} from './actions/actions.component';
import {SegmentComponent} from './segments/segment.component';
import {AuthGuard} from './guards/auth.guard';

const routes: Routes = [

  {path: 'login', component: LoginComponent},
  /*    { path: '', component: DashboardComponent, canActivate: [AuthGuard] },*/
  {path: '', redirectTo: '/eventdetectors', pathMatch: 'full'},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'eventdetectors', component: EventDetectorComponent, canDeactivate: [AuthGuard]},
  {path: 'suppressions', component: SuppressionComponent, canDeactivate: [AuthGuard]},
  {path: 'usecases', component: UsecaseComponent, canDeactivate: [AuthGuard]},
  {path: 'deploymentrequest', component: DeploymentRequestComponent},
  {path: 'deployment', component: DeploymentComponent},
  {path: 'pendingapproval', component: PendingApprovalComponent},
  {path: 'actions', component: ActionsComponent},
  {path: 'segments', component: SegmentComponent, canDeactivate: [AuthGuard]},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

export const routedComponents = [DashboardComponent];
