import {Http, BaseRequestOptions, Response, ResponseOptions, RequestMethod} from '@angular/http';
import {HttpService} from '../services/http-interceptor.service';
import {MockBackend, MockConnection} from '@angular/http/testing';
import {EventDetector} from '../eventdetectors/event-detectors';
import {TimeWindow} from '../models/timeWindow';
import {Suppression} from '../suppression/suppression';

export let fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HttpService,
    useFactory: (backend: MockBackend, options: BaseRequestOptions) => {
        // configure fake backend
        backend.connections.subscribe((connection: MockConnection) => {
            let testUser = {username: 'test', password: 'test', firstName: 'Test', lastName: 'User'};
            // wrap in timeout to simulate server api call
//            setTimeout(() => {
                // fake authenticate api end point
                if (connection.request.url.endsWith('/api/authenticate') && connection.request.method === RequestMethod.Post) {
                    // get parameters from post request
                    let params = JSON.parse(connection.request.getBody());

                    // check user credentials and return fake jwt token if valid
                    if (params.username === testUser.username && params.password === testUser.password) {
                        connection.mockRespond(new Response(
                            new ResponseOptions({status: 200, body: {token: 'fake-jwt-token'}})
                        ));
                    } else {
                        connection.mockRespond(new Response(
                            new ResponseOptions({status: 200})
                        ));
                    }
                }

                // fake users api end point
                if (connection.request.url.endsWith('/api/users') && connection.request.method === RequestMethod.Get) {
                    // check for fake auth token in header and return test users if valid, this security is implemented server side
                    // in a real application
                    if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                        connection.mockRespond(new Response(
                            new ResponseOptions({status: 200, body: [testUser]})
                        ));
                    } else {
                        // return 401 not authorised if token is null or invalid
                        connection.mockRespond(new Response(
                            new ResponseOptions({status: 401})
                        ));
                    }
                }

                // fake users api end point
                if (connection.request.url.endsWith('/ered/eventdetectors') && connection.request.method === RequestMethod.Get) {
                    // check for fake auth token in header and return test users if valid, this security is implemented server side
                    // in a real application
                    if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                        let eventDetectors: EventDetector[] = [];
                        let ed = new EventDetector();
                        ed.eventName = 'ACTIVATION';
                        ed.description = 'This is a real-time activation event when a new MSISDN is active';
                        ed.feedName = 'RECHARGE';
                        ed.eventType = 'Real Time';
                        ed.consumerName = 'ENBA';
                        ed.maxLatency = 2;
                        ed.maxLatencyUnit = 'days';
                        ed.version = 1.03;
                        ed.status = 'Production';
                        ed.priority = 1;
                        let timingList: TimeWindow[] = [];
                        let timing = new TimeWindow();
                        timing.startHrTime = "10";
                        timing.startMinTime = "50";
                        timing.startMeridian = "AM";
                        timing.endHrTime = "7";
                        timing.endMinTime = "45";
                        timing.endMeridian = "PM";
                        timing.timeWindowDay = "M";
                        timingList.push(timing);
                        timing = new TimeWindow();
                        timing.startHrTime = "08";
                        timing.startMinTime = "50";
                        timing.startMeridian = "AM";
                        timing.endHrTime = "11";
                        timing.endMinTime = "45";
                        timing.endMeridian = "PM";
                        timing.timeWindowDay = "Tu";
                        timingList.push(timing);
                        timing = new TimeWindow();
                        timing.startHrTime = "05";
                        timing.startMinTime = "15";
                        timing.startMeridian = "AM";
                        timing.endHrTime = "10";
                        timing.endMinTime = "45";
                        timing.endMeridian = "AM";
                        timing.timeWindowDay = "W";
                        timingList.push(timing);
                        ed.timeWindow = timingList;
                        let suppList: Suppression[] = [];
                        let supp = new Suppression();
                        supp.suppressionName = "R_ALL_CHECK_BL_MSISDN";
                        supp.suppressionId = 1;
                        supp.priority = 1;
                        suppList.push(supp);
                        supp = new Suppression();
                        supp.suppressionName = "R_ALL_CHECK_NEXT_CT_DATE";
                        supp.suppressionId = 2;
                        supp.priority = 2;
                        suppList.push(supp);
                        ed.suppressionRule = suppList;
                        eventDetectors.push(ed);
                        ed = new EventDetector();
                        ed.eventName = 'REALMONEY_LBA_REMINDER';
                        ed.description = 'This is a real-time REALMONEY_LBA_REMINDER event when a new MSISDN is active';
                        ed.feedName = 'REALMONEY_LBA';
                        ed.eventType = 'Real Time';
                        ed.consumerName = 'ECMP';
                        ed.maxLatency = 48;
                        ed.maxLatencyUnit = 'hours';
                        ed.version = 1.01;
                        ed.status = 'Production';
                        ed.priority = 2;
                        timingList = [];
                        timing = new TimeWindow();
                        timing.startHrTime = "10";
                        timing.startMinTime = "50";
                        timing.startMeridian = "AM";
                        timing.endHrTime = "7";
                        timing.endMinTime = "45";
                        timing.endMeridian = "PM";
                        timing.timeWindowDay = "M";
                        timingList.push(timing);
                        timing = new TimeWindow();
                        timing.startHrTime = "10";
                        timing.startMinTime = "50";
                        timing.startMeridian = "AM";
                        timing.endHrTime = "7";
                        timing.endMinTime = "45";
                        timing.endMeridian = "PM";
                        timing.timeWindowDay = "Tu";
                        timingList.push(timing);
                        timing = new TimeWindow();
                        timing.startHrTime = "10";
                        timing.startMinTime = "50";
                        timing.startMeridian = "AM";
                        timing.endHrTime = "7";
                        timing.endMinTime = "45";
                        timing.endMeridian = "PM";
                        timing.timeWindowDay = "W";
                        timingList.push(timing);
                        ed.timeWindow = timingList;
                        suppList = [];
                        supp = new Suppression();
                        supp.suppressionName = "R_ALL_TARIFF_COM_CHECK";
                        supp.suppressionId = 1;
                        supp.priority = 1;
                        suppList.push(supp);
                        supp = new Suppression();
                        supp.suppressionName = "R_RE_CHECK_LAST_ZMENTRY";
                        supp.suppressionId = 2;
                        supp.priority = 2;
                        suppList.push(supp);
                        ed.suppressionRule = suppList;
                        eventDetectors.push(ed);
                        ed = new EventDetector();
                        ed.eventName = 'REALMONEY_LBA_REMINDER';
                        ed.description = 'This is a real-time REALMONEY_LBA_REMINDER event when a new MSISDN is active';
                        ed.feedName = 'REALMONEY_LBA';
                        ed.eventType = 'Real Time';
                        ed.consumerName = 'ECMP';
                        ed.maxLatency = 48;
                        ed.maxLatencyUnit = 'hours';
                        ed.version = 1.01;
                        ed.status = 'Production';
                        ed.priority = 3;
                        ed.isInSimulation = true;
                        timingList = [];
                        timing = new TimeWindow();
                        timing.startHrTime = "10";
                        timing.startMinTime = "50";
                        timing.startMeridian = "AM";
                        timing.endHrTime = "7";
                        timing.endMinTime = "45";
                        timing.endMeridian = "PM";
                        timing.timeWindowDay = "M";
                        timingList.push(timing);
                        timing = new TimeWindow();
                        timing.startHrTime = "10";
                        timing.startMinTime = "50";
                        timing.startMeridian = "AM";
                        timing.endHrTime = "7";
                        timing.endMinTime = "45";
                        timing.endMeridian = "PM";
                        timing.timeWindowDay = "Tu";
                        timingList.push(timing);
                        timing = new TimeWindow();
                        timing.startHrTime = "10";
                        timing.startMinTime = "50";
                        timing.startMeridian = "AM";
                        timing.endHrTime = "7";
                        timing.endMinTime = "45";
                        timing.endMeridian = "PM";
                        timing.timeWindowDay = "W";
                        timingList.push(timing);
                        ed.timeWindow = timingList;
                        suppList = [];
                        supp = new Suppression();
                        supp.suppressionName = "R_ALL_CHECK_BL_MSISDN";
                        supp.suppressionId = 1;
                        supp.priority = 1;
                        suppList.push(supp);
                        supp = new Suppression();
                        supp.suppressionName = "R_RE_CHECK_LAST_ZMENTRY";
                        supp.suppressionId = 2;
                        supp.priority = 2;
                        suppList.push(supp);
                        ed.suppressionRule = suppList;
                        eventDetectors.push(ed);
                        ed = new EventDetector();
                        ed.eventName = 'REALMONEY_LBA_REMINDER';
                        ed.description = 'This is a real-time REALMONEY_LBA_REMINDER event when a new MSISDN is active';
                        ed.feedName = 'REALMONEY_LBA';
                        ed.eventType = 'Real Time';
                        ed.consumerName = 'ECMP';
                        ed.maxLatency = 48;
                        ed.maxLatencyUnit = 'hours';
                        ed.version = 1.01;
                        ed.status = 'Production';
                        ed.priority = 4;
                        ed.isInSimulation = false;
                        timingList = [];
                        timing = new TimeWindow();
                        timing.startHrTime = "10";
                        timing.startMinTime = "50";
                        timing.startMeridian = "AM";
                        timing.endHrTime = "7";
                        timing.endMinTime = "45";
                        timing.endMeridian = "PM";
                        timing.timeWindowDay = "M";
                        timingList.push(timing);
                        timing = new TimeWindow();
                        timing.startHrTime = "10";
                        timing.startMinTime = "50";
                        timing.startMeridian = "AM";
                        timing.endHrTime = "7";
                        timing.endMinTime = "45";
                        timing.endMeridian = "PM";
                        timing.timeWindowDay = "Tu";
                        timingList.push(timing);
                        timing = new TimeWindow();
                        timing.startHrTime = "10";
                        timing.startMinTime = "50";
                        timing.startMeridian = "AM";
                        timing.endHrTime = "7";
                        timing.endMinTime = "45";
                        timing.endMeridian = "PM";
                        timing.timeWindowDay = "W";
                        timingList.push(timing);
                        ed.timeWindow = timingList;
                        suppList = [];
                        supp = new Suppression();
                        supp.suppressionName = "R_ALL_TARIFF_COM_CHECK";
                        supp.suppressionId = 1;
                        supp.priority = 1;
                        suppList.push(supp);
                        supp = new Suppression();
                        supp.suppressionName = "R_RE_CHECK_LAST_ZMENTRY";
                        supp.suppressionId = 2;
                        supp.priority = 2;
                        suppList.push(supp);
                        ed.suppressionRule = suppList;
                        eventDetectors.push(ed);
                        ed = new EventDetector();
                        ed.eventName = 'REALMONEY_LBA_REMINDER';
                        ed.description = 'This is a real-time REALMONEY_LBA_REMINDER event when a new MSISDN is active';
                        ed.feedName = 'REALMONEY_LBA';
                        ed.eventType = 'Real Time';
                        ed.consumerName = 'ECMP';
                        ed.maxLatency = 48;
                        ed.maxLatencyUnit = 'hours';
                        ed.version = 1.01;
                        ed.status = 'Production';
                        ed.priority = 5;
                        ed.isInProduction = true;
                        timingList = [];
                        timing = new TimeWindow();
                        timing.startHrTime = "10";
                        timing.startMinTime = "50";
                        timing.startMeridian = "AM";
                        timing.endHrTime = "7";
                        timing.endMinTime = "45";
                        timing.endMeridian = "PM";
                        timing.timeWindowDay = "M";
                        timingList.push(timing);
                        timing = new TimeWindow();
                        timing.startHrTime = "10";
                        timing.startMinTime = "50";
                        timing.startMeridian = "AM";
                        timing.endHrTime = "7";
                        timing.endMinTime = "45";
                        timing.endMeridian = "PM";
                        timing.timeWindowDay = "Tu";
                        timingList.push(timing);
                        timing = new TimeWindow();
                        timing.startHrTime = "10";
                        timing.startMinTime = "50";
                        timing.startMeridian = "AM";
                        timing.endHrTime = "7";
                        timing.endMinTime = "45";
                        timing.endMeridian = "PM";
                        timing.timeWindowDay = "W";
                        timingList.push(timing);
                        ed.timeWindow = timingList;
                        suppList = [];
                        supp = new Suppression();
                        supp.suppressionName = "R_ALL_TARIFF_COM_CHECK";
                        supp.suppressionId = 1;
                        supp.priority = 1;
                        suppList.push(supp);
                        supp = new Suppression();
                        supp.suppressionName = "R_RE_CHECK_LAST_ZMENTRY";
                        supp.suppressionId = 2;
                        supp.priority = 2;
                        suppList.push(supp);
                        ed.suppressionRule = suppList;
                        eventDetectors.push(ed);
                        ed = new EventDetector();
                        ed.eventName = 'REALMONEY_LBA_REMINDER';
                        ed.description = 'This is a real-time REALMONEY_LBA_REMINDER event when a new MSISDN is active';
                        ed.feedName = 'REALMONEY_LBA';
                        ed.eventType = 'Real Time';
                        ed.consumerName = 'ECMP';
                        ed.maxLatency = 48;
                        ed.maxLatencyUnit = 'hours';
                        ed.version = 1.01;
                        ed.status = 'Production';
                        ed.priority = 6;
                        ed.isInProduction = true;
                        timingList = [];
                        timing = new TimeWindow();
                        timing.startHrTime = "10";
                        timing.startMinTime = "50";
                        timing.startMeridian = "AM";
                        timing.endHrTime = "7";
                        timing.endMinTime = "45";
                        timing.endMeridian = "PM";
                        timing.timeWindowDay = "M";
                        timingList.push(timing);
                        timing = new TimeWindow();
                        timing.startHrTime = "10";
                        timing.startMinTime = "50";
                        timing.startMeridian = "AM";
                        timing.endHrTime = "7";
                        timing.endMinTime = "45";
                        timing.endMeridian = "PM";
                        timing.timeWindowDay = "Tu";
                        timingList.push(timing);
                        timing = new TimeWindow();
                        timing.startHrTime = "10";
                        timing.startMinTime = "50";
                        timing.startMeridian = "AM";
                        timing.endHrTime = "7";
                        timing.endMinTime = "45";
                        timing.endMeridian = "PM";
                        timing.timeWindowDay = "W";
                        timingList.push(timing);
                        ed.timeWindow = timingList;
                        suppList = [];
                        supp = new Suppression();
                        supp.suppressionName = "R_ALL_TARIFF_COM_CHECK";
                        supp.suppressionId = 1;
                        supp.priority = 1;
                        suppList.push(supp);
                        supp = new Suppression();
                        supp.suppressionName = "R_RE_CHECK_LAST_ZMENTRY";
                        supp.suppressionId = 2;
                        supp.priority = 2;
                        suppList.push(supp);
                        ed.suppressionRule = suppList;
                        eventDetectors.push(ed);
                        connection.mockRespond(new Response(
                            new ResponseOptions({status: 200, body: eventDetectors})
                        ));
                    } else {
                        // return 401 not authorised if token is null or invalid
                        connection.mockRespond(new Response(
                            new ResponseOptions({status: 401})
                        ));
                    }
                }

                // fake users api end point
                if (connection.request.url.endsWith('/api/suppressions') && connection.request.method === RequestMethod.Get) {
                    // check for fake auth token in header and return test users if valid, this security is implemented server side
                    // in a real application
                    if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                        let suppressionList: Suppression[] = [];
                        let suppression = new Suppression();
                        suppression.suppressionId = 1;
                        suppression.suppressionName = "R-RECH_CHECK_TARIFF_ID_ST";
                        suppression.suppressionDesc = "Suppress if TARIFF ID does not start with 100 or 200";
                        suppression.failureLogLabel = "TARIF ID 100 or 200";
                        suppression.successLogLabel = "TARIF ID 100 or 200";
                        suppression.version = 1.20;
                        suppression.status = "Published";
                        suppressionList.push(suppression);
                        suppression = new Suppression();
                        suppression.suppressionId = 2;
                        suppression.suppressionName = "R_RECH_CHECL_MBB_PLAN";
                        suppression.suppressionDesc = "Suppress if a mobile broadband plan";
                        suppression.failureLogLabel = "Not MBB Plan";
                        suppression.successLogLabel = "MBB Plan";
                        suppression.version = 1.0;
                        suppression.status = "Published";
                        suppressionList.push(suppression);
                        suppression = new Suppression();
                        suppression.suppressionId = 3;
                        suppression.suppressionName = "R_ALL_OPEN_CMP_IND";
                        suppression.suppressionDesc = "Suppress if customer is in a Zoom campaign";
                        suppression.failureLogLabel = "";
                        suppression.successLogLabel = "In Zoom Campaign";
                        suppression.version = 1.1;
                        suppression.status = "Published";
                        suppressionList.push(suppression);
                        connection.mockRespond(new Response(
                            new ResponseOptions({status: 200, body: suppressionList})
                        ));
                    } else {
                        // return 401 not authorised if token is null or invalid
                        connection.mockRespond(new Response(
                            new ResponseOptions({status: 401})
                        ));
                    }
                }
                // fake users api end point
                if (connection.request.url.endsWith('/ered/suppressionrules') && connection.request.method === RequestMethod.Get) {
                    //                    if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    let suppressionList: Suppression[] = [];
                    let suppression = new Suppression();
                    suppression.suppressionId = 1;
                    suppression.suppressionName = "R-RECH_CHECK_TARIFF_ID_ST";
                    suppression.suppressionDesc = "Suppress if TARIFF ID does not start with 100 or 200";
                    suppression.failureLogLabel = "TARIF ID 100 or 200";
                    suppression.successLogLabel = "TARIF ID 100 or 200";
                    suppression.version = 1.20;
                    suppression.status = "Published";
                    suppression.isInSimulation = false;
                    suppression.isInProduction = false;
                    suppression.updatable = true;
                    suppression.criteriaExpression = "{\"operator\":\"AND\",\"rules\":[{\"condition\":\"=\",\"field\":\"supp add\",\"data\":\"63\"}]}";
                    suppressionList.push(suppression);
                    suppression = new Suppression();
                    suppression.suppressionId = 2;
                    suppression.suppressionName = "R_RECH_CHECL_MBB_PLAN";
                    suppression.suppressionDesc = "Suppress if a mobile broadband plan";
                    suppression.failureLogLabel = "Not MBB Plan";
                    suppression.successLogLabel = "MBB Plan";
                    suppression.version = 1.0;
                    suppression.status = "Published";
                    suppression.isInSimulation = false;
                    suppression.isInProduction = false;
                    suppression.updatable = true;
                    suppression.criteriaExpression = "{\"operator\":\"AND\",\"rules\":[{\"condition\":\"=\",\"field\":\"supp add\",\"data\":\"63\"}]}";
                    suppressionList.push(suppression);
                    suppression = new Suppression();
                    suppression.suppressionId = 3;
                    suppression.suppressionName = "R_ALL_OPEN_CMP_IND";
                    suppression.suppressionDesc = "Suppress if customer is in a Zoom campaign";
                    suppression.failureLogLabel = "";
                    suppression.successLogLabel = "In Zoom Campaign";
                    suppression.version = 1.1;
                    suppression.status = "Published";
                    suppression.isInSimulation = false;
                    suppression.isInProduction = false;
                    suppression.updatable = true;
                    suppression.criteriaExpression = "{\"operator\":\"AND\",\"rules\":[{\"condition\":\"=\",\"field\":\"supp add\",\"data\":\"63\"}]}";
                    suppressionList.push(suppression);
                    connection.mockRespond(new Response(
                        new ResponseOptions({status: 200, body: suppressionList})
                    ));
                    //                    } else {
                    //                        // return 401 not authorised if token is null or invalid
                    //                        connection.mockRespond(new Response(
                    //                            new ResponseOptions({status: 401})
                    //                        ));
                    //                    }
                }
                // fake users api end point
                if (connection.request.url.endsWith('/ered/suppressionrules') && connection.request.method === RequestMethod.Post) {
                    let suppression = JSON.parse(connection.request.getBody());
                    suppression.suppressionId = 4;
                    connection.mockRespond(new Response(
                        new ResponseOptions({status: 200, body: suppression})
                    ));
                }
                
                if (connection.request.url.endsWith('/ered/suppressionrules') && connection.request.method === RequestMethod.Put) {
                    let suppression = JSON.parse(connection.request.getBody());
                    connection.mockRespond(new Response(
                        new ResponseOptions({status: 200, body: suppression})
                    ));
                }
                if (connection.request.url.includes('/ered/suppressionrules/') && connection.request.method === RequestMethod.Delete) {
                    connection.mockRespond(new Response(
                        new ResponseOptions({status: 200})
                    ));
                }
                if (connection.request.url.includes('ered/archive/suppressionrules/') && connection.request.method === RequestMethod.Put) {
                    connection.mockRespond(new Response(
                        new ResponseOptions({status: 200})
                    ));
                }

//            }, 500);

        });

        return new HttpService(backend, options);
    },
    deps: [MockBackend, BaseRequestOptions]
};