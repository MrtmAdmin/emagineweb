/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import {Component, OnInit, AfterViewInit} from '@angular/core';
import {DeploymentRequestService} from './deploymentrequest.service';
import {DeploymentRequest} from './deploymentrequest';
import {UsecaseService} from '../usecases/usecases.service';
import {ContextMenuService} from '../directives/angular2-contextmenu/src/contextMenu.service';

@Component({
    moduleId: module.id,
    selector: 'deployment',
    templateUrl: 'deployment.component.html',

})
export class DeploymentComponent implements OnInit, AfterViewInit {
    
    constructor(private deploymentRequestService: DeploymentRequestService, private usecaseService: UsecaseService, private contextMenuService: ContextMenuService) {};
    
    approvedDeployment: DeploymentRequest[] = [];
    pendingDeployment: DeploymentRequest[] = [];
    
    
    ngAfterViewInit() {

    }

    ngOnInit(): void {
        this.getAllApprovedDeployment();
        this.getAllPendingDeployment();
    }
    
    getAllApprovedDeployment(){
        this.approvedDeployment = [];
        let dep = new DeploymentRequest();
        dep.deploymentRequestId = 1;
        dep.packageId = "PROD_345";
        dep.deploymentRequestName = "Name 1";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Awaiting Approval";
        this.approvedDeployment.push(dep);
        dep = new DeploymentRequest();
        dep.deploymentRequestId = 2;
        dep.packageId = "PROD_1290";
        dep.deploymentRequestName = "Name 2";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Awaiting Approval";
        this.approvedDeployment.push(dep);
        dep = new DeploymentRequest();
        dep.deploymentRequestId = 3;
        dep.packageId = "PROD_1277";
        dep.deploymentRequestName = "Name 3";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Awaiting Approval";
        this.approvedDeployment.push(dep);
        dep = new DeploymentRequest();
        dep.deploymentRequestId = 4;
        dep.packageId = "PROD_349";
        dep.deploymentRequestName = "Name 4";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Awaiting Approval";
        this.approvedDeployment.push(dep);
        dep = new DeploymentRequest();
        dep.deploymentRequestId = 5;
        dep.packageId = "PROD_231";
        dep.deploymentRequestName = "Name 5";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Awaiting Approval";
        this.approvedDeployment.push(dep);
    }
    
    getAllPendingDeployment(){
        this.pendingDeployment = [];
        let dep = new DeploymentRequest();
        dep.deploymentRequestId = 1;
        dep.packageId = "PROD_976";
        dep.deploymentRequestName = "Name 1";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Awaiting Approval";
        this.pendingDeployment.push(dep);
        dep = new DeploymentRequest();
        dep.deploymentRequestId = 2;
        dep.packageId = "PROD_1156";
        dep.deploymentRequestName = "Name 2";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Awaiting Approval";
        this.pendingDeployment.push(dep);
        dep = new DeploymentRequest();
        dep.deploymentRequestId = 3;
        dep.packageId = "PROD_1267";
        dep.deploymentRequestName = "Name 3";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Awaiting Approval";
        this.pendingDeployment.push(dep);
        dep = new DeploymentRequest();
        dep.deploymentRequestId = 4;
        dep.packageId = "PROD_1298";
        dep.deploymentRequestName = "Name 4";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Awaiting Approval";
        this.pendingDeployment.push(dep);
        dep = new DeploymentRequest();
        dep.deploymentRequestId = 5;
        dep.packageId = "PROD_1235";
        dep.deploymentRequestName = "Name 5";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Awaiting Approval";
        this.pendingDeployment.push(dep);
        dep = new DeploymentRequest();
        dep.deploymentRequestId = 6;
        dep.packageId = "PROD_1641";
        dep.deploymentRequestName = "Name 6";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Requested Deployment";
        this.pendingDeployment.push(dep);
        dep = new DeploymentRequest();
        dep.deploymentRequestId = 7;
        dep.packageId = "PROD_1642";
        dep.deploymentRequestName = "Name 7";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Requested Deployment";
        this.pendingDeployment.push(dep);
    }
    
    onCancel(status,item,index){
        if (status == "Approved"){
            this.approvedDeployment.splice(index,1);
        }else if(status == "Pending"){
            this.pendingDeployment.splice(index,1);
        }
    }
    
    onDeploy(item){
        console.log("item :" + JSON.stringify(item));
    }
}
