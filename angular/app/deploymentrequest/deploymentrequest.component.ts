/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import {Component, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import {DeploymentRequestService} from './deploymentrequest.service';
import {DeploymentRequest} from './deploymentrequest';
import {UsecaseService} from '../usecases/usecases.service';
import {UseCase} from '../usecases/usecases';
import {EventDetector} from '../eventdetectors/event-detectors';
import {ContextMenuService} from '../directives/angular2-contextmenu/src/contextMenu.service';
import {DataTable} from 'primeng/components/datatable/datatable';
import * as moment from 'moment/moment';

@Component({
    moduleId: module.id,
    selector: 'deployment-request',
    templateUrl: 'deploymentrequest.component.html',

})
export class DeploymentRequestComponent implements OnInit, AfterViewInit {

    @ViewChild('usecaseTbl')
    usecaseTbl: DataTable;

    constructor(private deploymentRequestService: DeploymentRequestService, private usecaseService: UsecaseService, private contextMenuService: ContextMenuService) {};
    useCases: UseCase[] = [];
    selectedUseCases: UseCase[] = [];
    selectedTab: any = 1;
    realTime: EventDetector[] = [];
    originalRealTime: EventDetector[] = [];
    batchEds: EventDetector[] = [];
    originalBatchEds: EventDetector[] = [];
    deploymentRequestObj = new DeploymentRequest();
    days = Array.from(Array(31), (e, i) => i);
    months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    years = [];
    submittedFlag: boolean = false;
    isValidDeployment: boolean = false;
    deploymentReqs: DeploymentRequest[] = [];
    showOnlyChanges:boolean = true;
    priorityConfirmDisplay:boolean = false;

    tabs = [{id: 1, title: "1. Deploy to Production", active: true}, {id: 2, title: "2. View Changes", active: false}, {id: 3, title: "3. Prioritise", active: false}, {id: 4, title: "4. Schedule Deployment", active: false}, {id: 5, title: "5. Approval", active: false}];

    ngAfterViewInit() {
        $('li.shrink').on('click', function (e) {
            e.stopPropagation();
            if ($(this).find('.shrink-arrow:first').hasClass('fa-caret-right')) {
                $(this).find('.shrink-arrow:first').removeClass('fa-caret-right');
                $(this).find('.shrink-arrow:first').addClass('fa-caret-down');
            }
            else {
                $(this).find('.shrink-arrow:first').addClass('fa-caret-right');
                $(this).find('.shrink-arrow:first').removeClass('fa-caret-down');
            }
            $(this).find('ul:first').slideToggle();
        })
    }

    clickEvent(e,id) {
        e.stopPropagation();
        if ($('#'+id).find('.shrink-arrow:first').hasClass('fa-caret-right')) {
            $('#'+id).find('.shrink-arrow:first').removeClass('fa-caret-right');
            $('#'+id).find('.shrink-arrow:first').addClass('fa-caret-down');
        }
        else {
            $('#'+id).find('.shrink-arrow:first').addClass('fa-caret-right');
            $('#'+id).find('.shrink-arrow:first').removeClass('fa-caret-down');
        }
        $('#'+id).find('ul:first').slideToggle();
    }

    ngOnInit(): void {
        var d = new Date();
        for (let i = 2016, max = d.getFullYear() + 10; i <= max; i++) {
            this.years.push(i);
        }
        this.getAllUseCases();
    }

    selectNav(tab, $event, deploymentRequestForm) {
        console.log(deploymentRequestForm)
        console.log("select navs")
        if (this.selectedUseCases.length > 0) {
            if (tab.id == 4) {
                var rtEdsMatched = this.compareRealTimeEd();
                var batchEdsMatched = this.compareBatchEd();
                if (rtEdsMatched || batchEdsMatched) {
                    this.deselectTab();
                    tab.active = true;
                    this.selectedTab = tab.id;
                } else {
                    console.log("else")
                    $event.stopPropagation();
                    $event.preventDefault();
                }
            } else if (tab.id == 3) {
                if (this.selectedUseCases.length > 0) {
                    this.originalRealTime = [];
                    this.originalBatchEds = [];
                    this.realTime = [];
                    this.batchEds = [];
                    for (let item of this.selectedUseCases) {
                        for (let data of item.edList) {
                            if (data.eventType == "REALTIME") {
                                var cnt = 0;
                                for (let eds of this.originalRealTime) {
                                    if (eds.eventName == data.eventName) {
                                        cnt++;
                                    }
                                }
                                if (cnt == 0) {
                                    this.originalRealTime.push(data);
                                }
                            } else if (data.eventType == "BATCH") {
                                var cnt = 0;
                                for (let eds of this.originalBatchEds) {
                                    if (eds.eventName == data.eventName) {
                                        cnt++;
                                    }
                                }
                                if (cnt == 0) {
                                    this.originalBatchEds.push(data);
                                }
                            }

                        }

                    }
                    Object.assign(this.realTime, this.originalRealTime);
                    Object.assign(this.batchEds, this.originalBatchEds);
                }
                this.deselectTab();
                tab.active = true;
                this.selectedTab = tab.id;
            } else if (tab.id == 5) {
                if (deploymentRequestForm.form.valid) {
                    this.deselectTab();
                    tab.active = true;
                    this.selectedTab = tab.id;
                } else {
                    $event.stopPropagation();
                    $event.preventDefault();
                }
            } else {
                this.deselectTab();
                tab.active = true;
                this.selectedTab = tab.id;
            }
        } else {
            $event.stopPropagation();
            $event.preventDefault();
        }

    }

    clearObjects() {
        this.selectedUseCases = [];
        if (this.usecaseTbl != null && this.usecaseTbl.selection != null) {
            delete this.usecaseTbl.selection;
        }
    }

    validateDeployment(deploymentRequestForm) {

        this.submittedFlag = true;
        if (deploymentRequestForm.form.valid) {
            this.submittedFlag = false;
            this.isValidDeployment = true;
        }
    }
    getAllUseCases() {
        let ctrl = this;
        this.usecaseService.get(0, 2000)
            .then(function (usecase) {
                Object.assign(ctrl.useCases, usecase);
            });
    }

    getAllPendingDeploymentReq() {
        this.deploymentReqs = [];
        let dep = new DeploymentRequest();
        dep.deploymentRequestId = 1;
        dep.packageId = "PROD_976";
        dep.deploymentRequestName = "Name 1";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Awaiting Approval";
        this.deploymentReqs.push(dep);
        dep = new DeploymentRequest();
        dep.deploymentRequestId = 2;
        dep.packageId = "PROD_1156";
        dep.deploymentRequestName = "Name 2";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Awaiting Approval";
        this.deploymentReqs.push(dep);
        dep = new DeploymentRequest();
        dep.deploymentRequestId = 3;
        dep.packageId = "PROD_1267";
        dep.deploymentRequestName = "Name 3";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Awaiting Approval";
        this.deploymentReqs.push(dep);
        dep = new DeploymentRequest();
        dep.deploymentRequestId = 4;
        dep.packageId = "PROD_1298";
        dep.deploymentRequestName = "Name 4";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Awaiting Approval";
        this.deploymentReqs.push(dep);
        dep = new DeploymentRequest();
        dep.deploymentRequestId = 5;
        dep.packageId = "PROD_1235";
        dep.deploymentRequestName = "Name 5";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Awaiting Approval";
        this.deploymentReqs.push(dep);
        dep = new DeploymentRequest();
        dep.deploymentRequestId = 6;
        dep.packageId = "PROD_1641";
        dep.deploymentRequestName = "Name 6";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Requested Deployment";
        this.deploymentReqs.push(dep);
        dep = new DeploymentRequest();
        dep.deploymentRequestId = 7;
        dep.packageId = "PROD_1642";
        dep.deploymentRequestName = "Name 7";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Requested Deployment";
        this.deploymentReqs.push(dep);
    }

    deselectTab() {
        this.tabs.forEach((nav) => {
            nav.active = false;
        });
    }

    goToViewChanges() {
        console.log("view chages")
        this.deselectTab();
        this.tabs[1].active = true;
        this.selectedTab = 2;
        console.log(this.selectedUseCases);
        let idList = [];
        for (let item of this.selectedUseCases) {
            idList.push(item.useCaseId);
            for (let assn of item.associatedUseCases) {
                idList.push(assn.useCaseId);
            }
        }
        var ids = idList.filter(function (elem, index, self) {
            return index == self.indexOf(elem);
        })
        console.log(ids instanceof Array);
        let ctrl = this;
        this.usecaseService.getUseCaseComparisionAttr(ids).then(function (data) {
            console.log("data ::::" + JSON.stringify(data));
            for (let item of data) {
                for (let selected of ctrl.selectedUseCases) {
                    if (item.useCaseId == selected.useCaseId) {
                        selected.edsChanged = item.edsChanged;
                        selected.useCaseChanged = item.useCaseChanged;
                    }
                }
            }
        }).catch(function () {
            console.log("errorrrrr")
        });
    }

    goToPriorities() {
        this.deselectTab();
        this.tabs[2].active = true;
        this.selectedTab = 3;
        if (this.selectedUseCases.length > 0) {
            this.originalRealTime = [];
            this.originalBatchEds = [];
            this.realTime = [];
            this.batchEds = [];
            for (let item of this.selectedUseCases) {
                for (let data of item.edList) {
                    if (data.eventType == "REALTIME") {
                        var cnt = 0;
                        for (let eds of this.originalRealTime) {
                            if (eds.eventName == data.eventName) {
                                cnt++;
                            }
                        }
                        if (cnt == 0) {
                            this.originalRealTime.push(data);
                        }
                    } else if (data.eventType == "BATCH") {
                        var cnt = 0;
                        for (let eds of this.originalBatchEds) {
                            if (eds.eventName == data.eventName) {
                                cnt++;
                            }
                        }
                        if (cnt == 0) {
                            this.originalBatchEds.push(data);
                        }
                    }

                }

            }
            Object.assign(this.realTime, this.originalRealTime);
            Object.assign(this.batchEds, this.originalBatchEds);
        }
    }

    goToScheduleDeployment() {
        console.log("go to")
        var rtEdsMatched = this.compareRealTimeEd();
        var batchEdsMatched = this.compareBatchEd();
        console.log("rtEdsMatched :" + rtEdsMatched)
        console.log("batchEdsMatched :" + batchEdsMatched)
        if (rtEdsMatched || batchEdsMatched) {
            this.deselectTab();
            this.tabs[3].active = true;
            this.selectedTab = 4;
        } else {
            console.log("doesn't match");
            this.openPriorityConfirmation();
        }
    }

    goToApproval(deploymentRequestForm) {
        this.submittedFlag = true;
        if (deploymentRequestForm.form.valid) {
            this.submittedFlag = false;
            this.deselectTab();
            this.tabs[4].active = true;
            this.selectedTab = 5;
            this.getAllPendingDeploymentReq();
        }
    }

    onDone() {
        console.log("done");
    }

    goToBack() {
        this.deselectTab();
        console.log("this.selectedTab - 1 ::::" + this.selectedTab)
        let tab = this.selectedTab - 2;
        this.tabs[tab].active = true;
        this.selectedTab = this.selectedTab - 1;
        this.isValidDeployment = false;
    }

    compareRealTimeEd() {
        for (var i = 0; i < this.originalRealTime.length; i++) {
            // Search every object in the job.data array for a match.
            // If found return false to remove this object from the results
            if (this.realTime[i].eventName !== this.originalRealTime[i].eventName) {
                return true;
            }
        }
        return false;
    }

    compareBatchEd() {
        for (var i = 0; i < this.originalBatchEds.length; i++) {
            // Search every object in the job.data array for a match.
            // If found return false to remove this object from the results
            if (this.batchEds[i].eventName !== this.originalBatchEds[i].eventName) {
                return true;
            }
        }
        return false;
    }


    onRowSelect($event) {
        console.log($event.data.useCaseId);
        console.log(this.selectedUseCases);
        let ctrl = this;
        this.usecaseService.getAssociatedUseCases($event.data.useCaseId)
            .then(function (usecases) {
                console.log("usecases ::::" + usecases);
                if (usecases.length > 0) {
                    for (let item of ctrl.selectedUseCases) {
                        if (item.useCaseId == $event.data.useCaseId) {
                            let assUsecase = [];
                            assUsecase = ctrl.useCases.filter(function (el) {
                                return usecases.indexOf(el.useCaseId) > -1;
                            });
                            item.associatedUseCases = assUsecase;
                            item.assnCnt = usecases.length;
                        }
                    }
                }
            }).catch(function () {
                console.log("errorrrrr")
            });
    }

    openPriorityConfirmation() {
        this.priorityConfirmDisplay = true;
    }

    moveToScheduleDeployment() {
        this.priorityConfirmDisplay = false;
        this.deselectTab();
        this.tabs[3].active = true;
        this.selectedTab = 4;

    }

    cancelPendingApproval(pendingReq, index) {
        console.log("canceled" + JSON.stringify(pendingReq));
        this.deploymentReqs.splice(index, 1);
    }

}

