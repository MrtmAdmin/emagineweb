/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import {EventDetector} from '../eventdetectors/event-detectors';

export class UseCase {
    public useCaseId:number;
    public useCaseName:string;
    public description:string;
    public status:string;
    public version:number;
    public owner:string;
    public useCaseEdAsnBO:UseCaseEdAsnBO[];
    public edList:EventDetector[];
    public updatable: boolean;
    public deletable: boolean;
    public archivable: boolean;
    public copyable: boolean;
    public versionable: boolean;
    public isInProduction: boolean;
    public isInSimulation: boolean;
    public lastUpdated: string;
    public lastUpdatedBy: string;
    public assnCnt: number;
    public associatedUseCases: UseCase[];
    public edsChanged:boolean;
    public useCaseChanged:boolean;
     public publishable:boolean;

    constructor() {
        this.updatable = true;
        this.deletable = false;
        this.archivable = false;
        this.copyable = false;
        this.versionable = false;
        this.assnCnt = 0;
        this.edList = [];
        this.associatedUseCases = [];
        this.edsChanged = false;
        this.useCaseChanged = false;
        this.publishable = true;
    }
}

export class UseCaseEdAsnBO {
    public eventDetectorId: number;
    // rcw:updated | Typo usecaseId to useCaseId
    public useCaseId: number;
    constructor() {};
}
