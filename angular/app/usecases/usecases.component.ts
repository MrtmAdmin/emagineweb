/*
 * This is the usecase component which
 * binds the usecase HTML with the usecase service.
 * All the logic related to Usecases are mentioned in this component file
 * @Author : Shreya
 */
import {Component, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import {EventDetectorService} from '../eventdetectors/event-detectors.service';
import {EventDetector} from '../eventdetectors/event-detectors';
import {Generic} from '../models/generic';
import {UsecaseService} from '../usecases/usecases.service';
import {UseCase} from '../usecases/usecases';
import {UseCaseEdAsnBO} from '../usecases/usecases';
import {ContextMenuService} from '../directives/angular2-contextmenu/src/contextMenu.service';
import {DataTable} from 'primeng/components/datatable/datatable';
import * as _ from "lodash";
import {Message} from 'primeng/primeng';

@Component({
    moduleId: module.id,
    selector: 'use-case',
    templateUrl: 'usecases.component.html',

})
export class UsecaseComponent implements OnInit, AfterViewInit {

    @ViewChild('configTable')
    configTable: DataTable;

    @ViewChild('simulTable')
    simulTable: DataTable;

    @ViewChild('prodTable')
    prodTable: DataTable;

    @ViewChild('usecaseForm') form;

    productionUseCaseList: UseCase[] = [];
    simulationUseCaseList: UseCase[] = [];
    configurationUseCaseList: UseCase[] = [];
    usecaseObj = new UseCase();
    showUpdateScreen: boolean = false;
    showEnlargedScreen: boolean = false;
    eventDetectorFlag: boolean = false;
    openAddPageFlag: boolean = false;
    showLargeListFlag: boolean = true;
    submittedFlag: boolean = false;
    dataSet: Generic[] = [];
    configMenuOptions = [];
    modalContent = new UseCase();
    selectedTab: string = "produc";
    selectedIndex: any = -1;
    isRequesting: boolean;
    archiveUsecaseDisplay: boolean = false;
    deleteUsecaseDisplay: boolean = false;
    cancelUsecaseDisplay: boolean = false;
    cancelChangesFlag: string = '';
    ruleBuilderUsecaseDisplay: boolean = false;
    popupIndex: number = -1;
    dirtyFlag: boolean = false;
    msgs: Message[] = [];
    usecaseListTabs = [
        {id: 1, title: "Production", active: true, shortName: "produc"},
        {id: 2, title: "Simulation", active: false, shortName: "simul"},
        {id: 3, title: "Configuration", active: false, shortName: "config"}];

    constructor(
        private usecaseService: UsecaseService,
        private eventDetectorService: EventDetectorService,
        private contextMenuService: ContextMenuService
    ) {};

    hasChanges() {
        return this.form.dirty || this.dirtyFlag;
    }

    ngOnInit(): void {
        this.getAllUsecases();
        this.getAllEventDetectors();
    }

    ngAfterViewInit() {
        $('body').css('overflow', '');
    }

    // rcw:comment Shreya |  - Setting the current selected Tab
    setTabParameters(tabName, index) {
        if (index > -1) {
            for (let i = 0; i < this.usecaseListTabs.length; i++) {
                if (i == index) {
                    this.usecaseListTabs[i].active = true;
                } else {
                    this.usecaseListTabs[i].active = false;
                }
            }
        }
        this.selectedTab = tabName;
    }

    // rcw:comment Shreya |  - Open Archive Modal Confirmation Popup
    openArchiveConfirmation(item) {
        this.modalContent = _.cloneDeep(item);
        this.archiveUsecaseDisplay = true;
    }

    // rcw:comment Shreya |  - Open Delete Modal Confirmation Popup
    openDeleteConfirmation(item) {
        this.modalContent = _.cloneDeep(item);
        this.deleteUsecaseDisplay = true;
    }
    // rcw:comment Shreya |  - Open Cancel Modal Confirmation Popup
    openCancelConfirmation(item, cancelChanges, index) {
        this.modalContent = _.cloneDeep(item);
        this.cancelUsecaseDisplay = true;
        this.cancelChangesFlag = cancelChanges;
        this.popupIndex = index;
    }

    // rcw:comment Shreya |  - Archiving the usecases by the clicked Usecase ID
    archiveUseCase() {
        var ctrl = this;
        this.isRequesting = true;
        this.usecaseService.archive(ctrl.modalContent.useCaseId).then(function () {
            ctrl.msgs.push({severity: 'success', summary: 'UseCase', detail: 'UseCase archived successfuly'});

            // Removing the selected usecase from the selected tab's usecase list.
            if (ctrl.selectedTab == "config") {
                for (let i = 0; i < ctrl.configurationUseCaseList.length; i++) {
                    if (ctrl.configurationUseCaseList[i].useCaseId == ctrl.modalContent.useCaseId) {
                        ctrl.configurationUseCaseList.splice(i, 1);
                        i--;
                        break;
                    }
                }
            } else if (ctrl.selectedTab == "simul") {
                for (let i = 0; i < ctrl.simulationUseCaseList.length; i++) {
                    if (ctrl.simulationUseCaseList[i].useCaseId == ctrl.modalContent.useCaseId) {
                        ctrl.simulationUseCaseList.splice(i, 1);
                        i--;
                        break;
                    }
                }
            } else if (ctrl.selectedTab == "produc") {
                for (let i = 0; i < ctrl.productionUseCaseList.length; i++) {
                    if (ctrl.productionUseCaseList[i].useCaseId == ctrl.modalContent.useCaseId) {
                        ctrl.productionUseCaseList.splice(i, 1);
                        i--;
                        break;
                    }
                }
            }

        }).then(function () {
            if (ctrl.modalContent.useCaseId == ctrl.usecaseObj.useCaseId) {
                ctrl.clearObjects();
            }
            ctrl.modalContent = new UseCase();
            ctrl.isRequesting = false;
        }).catch(function (error) {
            if (error && error._body) {
                ctrl.msgs.push({severity: 'error', summary: 'UseCase', detail: error._body});
            } else {
                ctrl.msgs.push({severity: 'error', summary: 'UseCase', detail: 'UseCase archive failed'});
                ctrl.modalContent = new UseCase();
            }
            ctrl.isRequesting = false;
        });
        this.archiveUsecaseDisplay = false;
    }

    // rcw:comment Shreya |  - Deleting the usecases by the clicked Usecase ID
    deleteUseCase() {
        var ctrl = this;
        this.isRequesting = true;
        this.usecaseService.delete(ctrl.modalContent.useCaseId).then(function () {
            ctrl.msgs.push({severity: 'success', summary: 'UseCase', detail: 'UseCase deleted successfuly'});

            // Removing the selected usecase from the selected tab's usecase list.
            if (ctrl.selectedTab == "config") {
                for (let i = 0; i < ctrl.configurationUseCaseList.length; i++) {
                    if (ctrl.configurationUseCaseList[i].useCaseId == ctrl.modalContent.useCaseId) {
                        ctrl.configurationUseCaseList.splice(i, 1);
                        i--;
                        break;
                    }
                }
            } else if (ctrl.selectedTab == "simul") {
                for (let i = 0; i < ctrl.simulationUseCaseList.length; i++) {
                    if (ctrl.simulationUseCaseList[i].useCaseId == ctrl.modalContent.useCaseId) {
                        ctrl.simulationUseCaseList.splice(i, 1);
                        i--;
                        break;
                    }
                }
            } else if (ctrl.selectedTab == "produc") {
                for (let i = 0; i < ctrl.productionUseCaseList.length; i++) {
                    if (ctrl.productionUseCaseList[i].useCaseId == ctrl.modalContent.useCaseId) {
                        ctrl.productionUseCaseList.splice(i, 1);
                        i--;
                        break;
                    }
                }
            }
        }).then(function () {
            if (ctrl.modalContent.useCaseId == ctrl.usecaseObj.useCaseId) {
                ctrl.clearObjects();
            }
            ctrl.modalContent = new UseCase();
            ctrl.isRequesting = false;
        }).catch(function (error) {
            if (error && error._body) {
                ctrl.msgs.push({severity: 'error', summary: 'UseCase', detail: error._body});
            } else {
                ctrl.msgs.push({severity: 'error', summary: 'UseCase', detail: 'UseCase deletion failed'});
                ctrl.modalContent = new UseCase();
            }
            ctrl.isRequesting = false;
        });
        this.deleteUsecaseDisplay = false;
    }

    checkCancelChanges() {
        if (this.selectedIndex !== -1) {
            this.openCancelConfirmation(this.usecaseObj, 'cancelSuppChanges', -1);
        } else {
            //If it is newly created usecase it clears the form
            this.clearObjects();
        }
    }

    // rcw:comment Shreya |  - Resetting the variables,flags and selected row/usecase from the list.
    clearObjects() {
        Object.keys(this.form.form.controls).forEach(control => {
            this.form.form.controls[control].markAsPristine();
        });
        this.usecaseObj = new UseCase();
        if (this.configTable != null && this.configTable.selection != null) {
            delete this.configTable.selection;
        }
        if (this.simulTable != null && this.simulTable.selection != null) {
            delete this.simulTable.selection;
        }
        if (this.prodTable != null && this.prodTable.selection != null) {
            delete this.prodTable.selection;
        }
        this.submittedFlag = false;
        this.selectedIndex = -1;
        this.dirtyFlag = false;
        this.cancelUsecaseDisplay = false;
    }

    // rcw:comment Shreya |  - Get All the Usecases from the server
    getAllUsecases() {
        this.isRequesting = true;
        var useCaseComp = this;
        this.productionUseCaseList = [];
        this.simulationUseCaseList = [];
        this.configurationUseCaseList = [];
        this.usecaseService.get(0, 2000)
            .then(function (usecase) {
                for (let item of usecase) {
                    if (item.isInProduction) {
                        useCaseComp.productionUseCaseList.push(item);
                    } else if (item.isInSimulation) {
                        useCaseComp.simulationUseCaseList.push(item);
                    }
                    useCaseComp.configurationUseCaseList.push(item)
                }
            }).then(function () {
                useCaseComp.isRequesting = false;
            }).catch(function () {
                useCaseComp.isRequesting = false;
            });
    }

    // rcw:comment Shreya |  - Get All the Event detectors which are of selected status from the server
    getAllEventDetectors() {
        this.isRequesting = true;
        var useCaseComp = this;
        this.eventDetectorService.get().then(function (eds) {
            //Preaparing the data list for the menu item to be displayed on the library panel
            for (let item of eds) {
                let edObj = new Generic();
                edObj.name = item.eventName;
                edObj.id = item.eventDetectionId
                edObj.parenttype = "EventDetectors";
                useCaseComp.dataSet.push(edObj);
            }
        }).then(function () {
            useCaseComp.isRequesting = false;
        }).catch(function () {
            useCaseComp.isRequesting = false;
        });
    }

    cancelUpdateChanges() {
        Object.keys(this.form.form.controls).forEach(control => {
            this.form.form.controls[control].markAsPristine();
        });
        this.usecaseObj = _.cloneDeep(this.modalContent);
        this.selectedIndex = this.popupIndex;
        this.showUpdateScreen = true;
        this.eventDetectorFlag = true;
        this.showLargeListFlag = false;
        this.dirtyFlag = false;
        //To reduce the list area and expand the config space
        $(".expand-div").removeClass("wide-div");
        $(".crud-div").css("display", "block");
        this.cancelUsecaseDisplay = false;
    }

    rejectSuppressionTraversalChanges() {
        if (this.selectedTab == "config") {
            delete this.configTable.selection;
            for (let item of this.configurationUseCaseList) {
                if (item.useCaseId == this.usecaseObj.useCaseId) {
                    this.configTable.selection = item;
                    break;
                }
            }
        } else if (this.selectedTab == "produc") {
            for (let item of this.productionUseCaseList) {
                if (item.useCaseId == this.usecaseObj.useCaseId) {
                    this.prodTable.selection = item;
                    break;
                }
            }
        } else if (this.selectedTab == "simul") {
            for (let item of this.simulationUseCaseList) {
                if (item.useCaseId == this.usecaseObj.useCaseId) {
                    this.simulTable.selection = item;
                    break;
                }
            }
        }
        this.cancelUsecaseDisplay = false;
    }

    // rcw:comment Shreya |  - open update form when selected any usecase
    openUpdateTemplate(es) {
        let usecase = es.data;
        var index = -1;
        //Retrieving the index of the usecase by indentifying the object from the server usecase list
        if (this.selectedTab == "config") {
            index = this.configurationUseCaseList.indexOf(usecase);
        } else if (this.selectedTab == "produc") {
            index = this.productionUseCaseList.indexOf(usecase);
        } else if (this.selectedTab == "simul") {
            index = this.simulationUseCaseList.indexOf(usecase);
        }

        //If object is present returns the index from the list and populates the usecase data onto the below config space
        console.log("open index :" + index);
        if (index !== -1) {
            if (this.form.dirty) {
                this.openCancelConfirmation(usecase, 'changeEditCancel', index);
            } else {
                Object.keys(this.form.form.controls).forEach(control => {
                    this.form.form.controls[control].markAsPristine();
                });
                this.usecaseObj = _.cloneDeep(usecase);
                this.selectedIndex = index;
                this.showUpdateScreen = true;
                this.eventDetectorFlag = true;
                this.showLargeListFlag = false;
                this.dirtyFlag = false;
                //To reduce the list area and expand the config space
                $(".expand-div").removeClass("wide-div");
                $(".crud-div").css("display", "block");
            }

        }

    }

    // rcw:comment Shreya |  - When user clicks on add button.Clears the config space form and also resets the flags and variable
    openAddPage() {
        //If user directly wants to enter to add page without going onto the update page and then clicks on add
        if (!this.showUpdateScreen) {
            this.openAddPageFlag = true;
            this.showEnlargedScreen = true;
            this.showUpdateScreen = true;
            //It increases the config space area from small section to whole screen and also the library sections
            $(".crud-div").animate({height: "toggle"});
            $("#tools").removeClass("fix-height-385").addClass("h-555");
        }
        this.clearObjects();
        this.showUpdateScreen = true;
        this.dirtyFlag = false;
    }

    // rcw:comment Shreya |  - Sets the flags when clicks on large/small toggle icon
    setLargeListFlag() {
        if (this.showLargeListFlag) {
            this.showLargeListFlag = false;
            this.showEnlargedScreen = false;
            this.showUpdateScreen = true;
        } else {
            this.showLargeListFlag = true;
            this.showUpdateScreen = false;
        }
    }

    // rcw:comment Shreya |  - Sets the flags when clicks on large/small toggle icon
    togglePage() {
        //when screen is enlarged set list view large and also add page must be enlarged
        if (this.showEnlargedScreen) {
            this.showEnlargedScreen = false;
            this.openAddPageFlag = false;
            $("#tools").removeClass("h-555").addClass("fix-height-385");
        } else {
            this.showEnlargedScreen = true;
            $("#tools").removeClass("fix-height-385").addClass("h-555");
        }
        $(".expand-div").removeClass("wide-div");
    }

    // rcw:comment Shreya |  - Associating event detector to the selected/new usecase using drag and drop.
    transferEventDetectorDataSuccess($event) {
        var count = 0;
        for (let item of this.usecaseObj.edList) {
            if ($event.id == item.eventDetectionId) {
                count++;
            }
        }
        if (count == 0) {
            let eventDetectorObj = new EventDetector();
            eventDetectorObj.eventName = $event.name;
            eventDetectorObj.eventDetectionId = $event.id;
            eventDetectorObj.priority = this.usecaseObj.edList.length + 1;
            this.usecaseObj.edList.push(eventDetectorObj);
            this.dirtyFlag = true;
        }
    }

    // rcw:comment Shreya |  - De-associate event detector to the selected/new usecase
    removeEventDetector(index) {
        this.usecaseObj.edList.splice(index, 1);
        this.dirtyFlag = true;
    }

    // rcw:comment Shreya |  - Add/update Usecase if it satisfies all the validation
    onSubmit(usecaseForm) {
        this.submittedFlag = true;
        if (usecaseForm.form.valid && this.usecaseObj.edList.length > 0) {
            this.isRequesting = true;
            this.submittedFlag = false;
            if (this.usecaseObj.edList != null && this.usecaseObj.edList.length > 0) {
                this.usecaseObj.useCaseEdAsnBO = [];
                //Preparing the association objects with eventdetectors and its related usecase.
                for (let item of this.usecaseObj.edList) {
                    let edAsn = new UseCaseEdAsnBO();
                    edAsn.useCaseId = this.usecaseObj.useCaseId;
                    edAsn.eventDetectorId = item.eventDetectionId;
                    this.usecaseObj.useCaseEdAsnBO.push(edAsn);
                }
            }
            console.log("this.usecaseObj :" + JSON.stringify(this.usecaseObj));
            //If it contains usecase ID then perform update of usecase.
            if (this.usecaseObj.useCaseId) {
                delete this.usecaseObj['_$visited'];
                var ctrl = this;
                this.usecaseService.update(this.usecaseObj).then(function (updatedUsecase) {
                    ctrl.msgs.push({severity: 'success', summary: 'UseCase', detail: 'UseCase updated successfuly'});
                    if (ctrl.selectedIndex == -1) {
                        ctrl.getAllUsecases();
                    } else {
                        //Update the selected usecase at the selected index of the selected tab's list
                        if (ctrl.selectedTab == "config") {
                            for (let i = 0; i < ctrl.configurationUseCaseList.length; i++) {
                                if (ctrl.configurationUseCaseList[i].useCaseId == updatedUsecase.useCaseId) {
                                    ctrl.configurationUseCaseList[i] = updatedUsecase;
                                    break;
                                }
                            }
                        } else if (ctrl.selectedTab == "simul") {
                            for (let i = 0; i < ctrl.simulationUseCaseList.length; i++) {
                                if (ctrl.simulationUseCaseList[i].useCaseId == updatedUsecase.useCaseId) {
                                    ctrl.simulationUseCaseList[i] = updatedUsecase;
                                    break;
                                }
                            }
                        } else if (ctrl.selectedTab == "produc") {
                            for (let i = 0; i < ctrl.productionUseCaseList.length; i++) {
                                if (ctrl.productionUseCaseList[i].useCaseId == updatedUsecase.useCaseId) {
                                    ctrl.productionUseCaseList[i] = updatedUsecase;
                                    break;
                                }
                            }
                        }
                    }
                }).then(function () {
                    Object.keys(ctrl.form.form.controls).forEach(control => {
                        ctrl.form.form.controls[control].markAsPristine();
                    });
                    ctrl.isRequesting = false;
                }).catch(function (error) {
                    if (error && error._body) {
                        ctrl.msgs.push({severity: 'error', summary: 'UseCase', detail: error._body});
                    } else {
                        ctrl.msgs.push({severity: 'error', summary: 'UseCase', detail: 'UseCase updation failed'});
                    }
                    ctrl.isRequesting = false;
                });
            } else {
                //If it doesn't contains the usecase ID it means its a newly created usecase so perform add operation
                this.usecaseObj.useCaseId = null;
                this.usecaseObj.status = "DRAFT";
                var ctrl = this;
                this.usecaseService.add(this.usecaseObj).then(function (addedUsecase) {
                    ctrl.msgs.push({severity: 'success', summary: 'UseCase', detail: 'UseCase created successfuly'});
                    //Add the newly created usecase in the selected tab's list
                    if (ctrl.selectedTab == "config") {
                        ctrl.configurationUseCaseList.push(addedUsecase);
                    } else if (ctrl.selectedTab == "simul") {
                        ctrl.simulationUseCaseList.push(addedUsecase);
                    } else if (ctrl.selectedTab == "produc") {
                        ctrl.productionUseCaseList.push(addedUsecase);
                    }
                }).then(function () {
                    ctrl.clearObjects();
                    ctrl.isRequesting = false;
                }).catch(function (error) {
                    if (error && error._body) {
                        ctrl.msgs.push({severity: 'error', summary: 'UseCase', detail: error._body});
                    } else {
                        ctrl.msgs.push({severity: 'error', summary: 'UseCase', detail: 'UseCase creation failed'});
                    }
                    ctrl.isRequesting = false;
                });

            }
        }
    }

    // rcw:comment Shreya |  - This method is for save and publish the usecase after all the validations
    onPublish(usecaseForm) {
        this.submittedFlag = true;
        if (usecaseForm.form.valid && this.usecaseObj.edList.length > 0) {
            this.isRequesting = true;
            this.submittedFlag = false;
            if (this.usecaseObj.edList != null && this.usecaseObj.edList.length > 0) {
                this.usecaseObj.useCaseEdAsnBO = [];
                for (let item of this.usecaseObj.edList) {
                    let edAsn = new UseCaseEdAsnBO();
                    edAsn.useCaseId = this.usecaseObj.useCaseId;
                    edAsn.eventDetectorId = item.eventDetectionId;
                    this.usecaseObj.useCaseEdAsnBO.push(edAsn);
                }
            }
            delete this.usecaseObj['_$visited'];
            var ctrl = this;
            this.usecaseService.publish(this.usecaseObj).then(function (updatedUsecase) {
                ctrl.msgs.push({severity: 'success', summary: 'UseCase', detail: 'UseCase published successfuly'});
                //If it is the newly created usecase adding that usecase in the selected tab's list
                if (ctrl.usecaseObj.useCaseId == null) {
                    if (ctrl.selectedTab == "config") {
                        ctrl.configurationUseCaseList.push(updatedUsecase);
                    } else if (ctrl.selectedTab == "simul") {
                        ctrl.simulationUseCaseList.push(updatedUsecase);
                    } else if (ctrl.selectedTab == "produc") {
                        ctrl.productionUseCaseList.push(updatedUsecase);
                    }
                } else {
                    //updating the selected usecase with the updated status and changes in the selected tab's list
                    if (ctrl.selectedIndex == -1) {
                        ctrl.getAllUsecases();
                    } else {
                        if (ctrl.selectedTab == "config") {
                            for (let i = 0; i < ctrl.configurationUseCaseList.length; i++) {
                                if (ctrl.configurationUseCaseList[i].useCaseId == updatedUsecase.useCaseId) {
                                    ctrl.configurationUseCaseList[i] = updatedUsecase;
                                    break;
                                }
                            }
                        } else if (ctrl.selectedTab == "simul") {
                            for (let i = 0; i < ctrl.simulationUseCaseList.length; i++) {
                                if (ctrl.simulationUseCaseList[i].useCaseId == updatedUsecase.useCaseId) {
                                    ctrl.simulationUseCaseList[i] = updatedUsecase;
                                    break;
                                }
                            }
                        } else if (ctrl.selectedTab == "produc") {
                            for (let i = 0; i < ctrl.productionUseCaseList.length; i++) {
                                if (ctrl.productionUseCaseList[i].useCaseId == updatedUsecase.useCaseId) {
                                    ctrl.productionUseCaseList[i] = updatedUsecase;
                                    break;
                                }
                            }
                        }
                    }
                }
            }).then(function () {
                Object.keys(ctrl.form.form.controls).forEach(control => {
                    ctrl.form.form.controls[control].markAsPristine();
                });
                ctrl.isRequesting = false;
            }).catch(function (error) {
                if (error && error._body) {
                    ctrl.msgs.push({severity: 'error', summary: 'UseCase', detail: error._body});
                } else {
                    ctrl.msgs.push({severity: 'error', summary: 'UseCase', detail: 'UseCase publishion failed'});
                }
                ctrl.isRequesting = false;
            });
        }
    }


    // rcw:comment Shreya |  - Cancels the changes on the usecase
    cancelUseCases() {
        //If the usecase is for updation cancels the new added changes
        Object.keys(this.form.form.controls).forEach(control => {
            this.form.form.controls[control].markAsPristine();
        });
        if (this.selectedIndex !== -1) {
            if (this.selectedTab == "config") {
                this.usecaseObj = _.cloneDeep(this.configurationUseCaseList[this.selectedIndex]);
            } else if (this.selectedTab == "simul") {
                this.usecaseObj = _.cloneDeep(this.simulationUseCaseList[this.selectedIndex]);
            } else if (this.selectedTab == "produc") {
                this.usecaseObj = _.cloneDeep(this.productionUseCaseList[this.selectedIndex]);
            }
            this.cancelUsecaseDisplay = false;
        } else {
            //If it is newly created usecase it clears the form
            this.clearObjects();
        }
    }

    // rcw:comment Shreya |  - Generates the dynamic context menu for the right click on the usecase name for the various actions like new version,Delete,Archive,Duplicate
    public onContextMenu($event: MouseEvent, item: any): void {
        this.configMenuOptions = [];
        this.configMenuOptions.push({
            html: () => 'Version',
            click: (item) => {
                //Creating the new version on menu select by usecase ID
                var ctrl = this;
                this.isRequesting = true;
                this.usecaseService.nextVersion(item.useCaseId).then(function (newVersionUsecase) {
                    ctrl.msgs.push({severity: 'success', summary: 'UseCase', detail: 'New Version created successfuly'});
                    if (ctrl.selectedTab == "config") {
                        ctrl.configurationUseCaseList.push(newVersionUsecase);
                    } else if (ctrl.selectedTab == "simul") {
                        ctrl.simulationUseCaseList.push(newVersionUsecase);
                    } else if (ctrl.selectedTab == "produc") {
                        ctrl.productionUseCaseList.push(newVersionUsecase);
                    }
                }).then(function () {
                    ctrl.clearObjects();
                    ctrl.isRequesting = false;
                }).catch(function (error) {
                    if (error && error._body) {
                        ctrl.msgs.push({severity: 'error', summary: 'UseCase', detail: error._body});
                    } else {
                        ctrl.msgs.push({severity: 'error', summary: 'UseCase', detail: 'New version creation failed'});
                    }
                    ctrl.isRequesting = false;
                });
            },
            enabled: (item): boolean => {
                //Gets enabled only if usecase have rights to create new version
                return item.versionable;
            }
        })
        this.configMenuOptions.push({
            html: () => 'Duplicate',
            click: (item) => {
                //Creating the duplicate of the usecase on menu select by usecase ID
                var ctrl = this;
                this.isRequesting = true;
                this.usecaseService.copy(item.useCaseId).then(function (duplicatedUsecase) {
                    ctrl.msgs.push({severity: 'success', summary: 'UseCase', detail: 'UseCase duplicated successfuly'});
                    if (ctrl.selectedTab == "config") {
                        ctrl.configurationUseCaseList.push(duplicatedUsecase);
                    } else if (ctrl.selectedTab == "simul") {
                        ctrl.simulationUseCaseList.push(duplicatedUsecase);
                    } else if (ctrl.selectedTab == "produc") {
                        ctrl.productionUseCaseList.push(duplicatedUsecase);
                    }
                }).then(function () {
                    ctrl.clearObjects();
                    ctrl.isRequesting = false;
                }).catch(function (error) {
                    if (error && error._body) {
                        ctrl.msgs.push({severity: 'error', summary: 'UseCase', detail: error._body});
                    } else {
                        ctrl.msgs.push({severity: 'error', summary: 'UseCase', detail: 'UseCase duplication failed'});
                    }
                    ctrl.isRequesting = false;
                });
            }, enabled: (item): boolean => {
                //Gets enabled only if usecase have rights to create duplicate
                return item.copyable;
            }
        })
        this.configMenuOptions.push({
            html: () => 'Archive',
            click: (item) => {
                //Open archive confirmation message to notify user
                this.openArchiveConfirmation(item);
            }, enabled: (item): boolean => {
                //Gets enabled only if usecase have rights to archive the usecase
                return item.archivable;
            }
        })
        this.configMenuOptions.push({
            html: () => 'Delete',
            click: (item) => {
                //Open delete confirmation message to notify user
                this.openDeleteConfirmation(item);
            }, enabled: (item): boolean => {
                //Gets enabled only if usecase have rights to delete the usecase
                return item.deletable;
            }
        })
        this.contextMenuService.show.next({
            actions: this.configMenuOptions,
            event: $event,
            item: item
        });
        $event.preventDefault();
    }

}

