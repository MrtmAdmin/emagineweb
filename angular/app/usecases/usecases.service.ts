/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import {Injectable} from '@angular/core';
import {HttpService} from '../services/http-interceptor.service';
import 'rxjs/add/operator/toPromise';
import {UseCase} from '../usecases/usecases';
import {Configuration} from '../services/configuration';

@Injectable()
export class UsecaseService {

    private actionUrl: string;
    constructor(private http: HttpService, private configuration: Configuration) {
        this.actionUrl = configuration.ServerWithApiUrl;
    }
    
    get(pageIndex:number,pageSize:number): Promise<UseCase[]> {
        return this.http
            .get(this.actionUrl + 'ered/page/usecases?pageIndex=' + pageIndex + '&pageSize=' + pageSize)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getById(id: number): Promise<UseCase> {
        return this.http.get(this.actionUrl +"ered/usecases/" + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    add(usecase: UseCase) : Promise<any> {
        return this.http.post(this.actionUrl +"ered/usecases", usecase)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    
    update(usecase: UseCase) : Promise<any> {
        return this.http.put(this.actionUrl +"ered/usecases", usecase)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    
    publish(usecase: UseCase) : Promise<any> {
        return this.http.post(this.actionUrl +"ered/publish/usecases/", usecase)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    
    copy(id: number): Promise<any> {
        console.log("id :"+id)
        return this.http.put(this.actionUrl +"ered/copy/usecases/"+id,"")
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    
    archive(id: number): Promise<any> {
        return this.http.put(this.actionUrl +"ered/archive/usecases/"+id,"")
            .toPromise()
            .then(response => response)
            .catch(this.handleError);
    }
    
    delete(id: number): Promise<any> {
        return this.http.delete(this.actionUrl +"ered/usecases/"+id)
            .toPromise()
            .then(response => response)
            .catch(this.handleError);
    }
    nextVersion(id: number): Promise<any> {
        return this.http.put(this.actionUrl +"ered/version/usecases/"+id, "")
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    
    getAssociatedUseCases(id: number): Promise<any> {
        return this.http.get(this.actionUrl +"ered/associates/usecases/"+id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    
    getUseCaseComparisionAttr(ids: any): Promise<any> {
        return this.http.get(this.actionUrl +"ered/comapre/usecases/["+ids+"]")
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    
     private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error);
    }
}

