/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {SegmentComponent} from './segment.component';
import {SegmentService} from './segment.service';
import {SharedModule} from '../shared/shared.module';
import {AppRoutingModule} from '../app-routing.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        AppRoutingModule
    ],
    declarations: [
        SegmentComponent
    ],
    providers: [SegmentService]
})
export class SegmentModule {

}


