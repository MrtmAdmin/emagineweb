/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import {Injectable} from '@angular/core';
import {HttpService} from '../services/http-interceptor.service';
import 'rxjs/add/operator/toPromise';
import {Segments} from '../segments/segment';
import {Configuration} from '../services/configuration';

@Injectable()
export class SegmentService {
    
    private actionUrl: string;
    constructor(private http: HttpService, private configuration: Configuration) {
        this.actionUrl = configuration.ServerWithApiUrl;
    }
    
    get(): Promise<Segments[]> {
        return this.http
            .get(this.actionUrl +'ered/segment')
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getById(id: number): Promise<Segments> {
        return this.http.get(this.actionUrl +"ered/segment/" + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    add(segment: Segments) : Promise<any> {
        return this.http.post(this.actionUrl +"ered/segment", segment)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    
    update(suppression: Segments) : Promise<any> {
        return this.http.put(this.actionUrl +"ered/segment", suppression)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    
    publish(segment: Segments) : Promise<any> {
        return this.http.post(this.actionUrl +"ered/publish/segment/", segment)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    
    copy(id: number): Promise<any> {
        console.log("id :"+id)
        return this.http.put(this.actionUrl +"ered/copy/segment/"+id,"")
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    
    archive(id: number): Promise<any> {
        return this.http.put(this.actionUrl +"ered/archive/segment/"+id,"")
            .toPromise()
            .then(response => response)
            .catch(this.handleError);
    }
    
    delete(id: number): Promise<any> {
        return this.http.delete(this.actionUrl +"ered/segment/"+id)
            .toPromise()
            .then(response => response)
            .catch(this.handleError);
    }
    
    nextVersion(id: number): Promise<any> {
        return this.http.put(this.actionUrl +"ered/version/segment/"+id, "")
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    
     private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error);
    }
}
