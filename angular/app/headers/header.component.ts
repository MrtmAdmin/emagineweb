import {Component, OnInit, AfterViewInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'header',
    templateUrl: 'header.component.html',
})
export class HeaderComponent implements OnInit, AfterViewInit {
    selectValue: string = 'en';
    ngOnInit(): void {
        
    }

    ngAfterViewInit() {
        //Submenu hover
        $('.subhoverLink').click(function (e) {
            $('.outer_submenu').stop();
            if ($($(this).data('id')).is(":hidden")) {
                $('.navsub').hide();
                $('.subhoverLink').removeClass('subhoverLinkTringle');
                $(this).addClass('subhoverLinkTringle');
                $($(this).data('id')).show().css('padding-left', $(this).position().left);
                $('.outer_submenu').slideDown();
            } else {
                $('.subhoverLink').removeClass('subhoverLinkTringle');
                $('.outer_submenu').slideUp();
            }
        });
        $(document).click(function (e) {
            if (!$(event.target).is('.subhoverLink')) {
                $('.subhoverLink').removeClass('subhoverLinkTringle');
                $('.outer_submenu').slideUp();

            }
        });
        setTimeout(() => {
            this.selectValue = localStorage.getItem('localeId');
        }, 1);

    }


    public onChange(localeId: string): void {
        localStorage.setItem('localeId', localeId);
        location.reload(false);
    }
}