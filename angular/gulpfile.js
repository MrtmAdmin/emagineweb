const gulp = require('gulp');
const del = require('del');
const typescript = require('gulp-typescript');
const tscConfig = require('./tsconfig.json');
const sourcemaps = require('gulp-sourcemaps');

// clean the contents of the distribution directory
gulp.task('clean', function () {
    return del('dist/**/*');
});

// TypeScript compile
gulp.task('compile', ['clean'], function () {
    return gulp
            .src("app/**/*.ts")
            .pipe(sourcemaps.init())          // <--- sourcemaps
            .pipe(typescript(tscConfig.compilerOptions))
            .pipe(sourcemaps.write('.'))      // <--- sourcemaps
            .pipe(gulp.dest('dist/'));
});

// TypeScript compile
gulp.task('system:config', ['compile'], function () {
    return gulp
            .src("system-config.ts")
            .pipe(sourcemaps.init())          // <--- sourcemaps
            .pipe(typescript(tscConfig.compilerOptions))
            .pipe(sourcemaps.write('.'))      // <--- sourcemaps
            .pipe(gulp.dest('dist'));
});

// copy dependencies
gulp.task('copy:libs',['system:config'], function () {
    return gulp.src([
        'core-js/client/shim.min.js',
        'zone.js/dist/zone.js',
        'systemjs/dist/system.src.js',
        'primeng/resources/themes/omega/theme.css',
        'primeng/resources/primeng.min.css',
        'reflect-metadata/Reflect.js',
        'rxjs/**',
        'zone.js/dist/**',
        '@angular/**',
        'ng2-dnd/**',
        'primeng/**',
        'moment/**',
        'angular2-toastr/**',
        'lodash/**'
    ], {cwd: "node_modules/**"})
            .pipe(gulp.dest('dist/lib'));
});

// copy static assets - i.e. non TypeScript compiled source
gulp.task('copy:assets',['copy:libs'], function () {
    return gulp.src(['app/**/*', 'index.html', 'assets/**/*', '!app/**/*.ts'], {base: './'})
            .pipe(gulp.dest('dist'));
});

/**
 * Watch for changes in TypeScript, HTML and CSS files.
 */
gulp.task('watch', function () {
    gulp.watch(["app/**/*.ts"], ['compile']).on('change', function (e) {
        console.log('TypeScript file ' + e.path + ' has been changed. Compiling.');
    });
    gulp.watch(["app/**/*'",'!app/**/*.ts', 'index.html','assets/**/*'], ['copy:assets']).on('change', function (e) {
        console.log('Resource file ' + e.path + ' has been changed. Updating.');
    });
});

gulp.task('build', ['copy:assets']);
gulp.task('default', ['build']);